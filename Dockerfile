FROM yobasystems/alpine-nginx
RUN apk --no-cache add curl
COPY ./www /etc/nginx/html
COPY ./data/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./data/runApp.sh ./runApp.sh
CMD ["nginx"]
RUN chmod 777 ./runApp.sh && chmod +x ./runApp.sh
CMD ["./runApp.sh"]